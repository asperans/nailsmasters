const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const merge = require('webpack-merge');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const src = { root: path.resolve(__dirname, 'src/') };
const dist = { root: path.resolve(__dirname, '../web/local/assets/') };

Object.assign(src, {
  img: path.resolve(src.root, 'img/'),
  font: path.resolve(src.root, 'font/'),
  ico: path.resolve(src.root, 'ico/'),
  pug: path.resolve(src.root, 'pug/'),
  js: path.resolve(src.root, 'js/')
});

const getEntries = platform => ({
  app: {
    name: `app-${platform}`,
    path: `./entry/${platform}/`
  },
  styl: {
    name: `styl-${platform}`,
    path: `./entry/${platform}/styl`
  },
  assets: {
    name: `assets-${platform}`,
    path: `./entry/${platform}/ico`
  },
  pug: {
    name: `pug-${platform}`,
    path: `./entry/${platform}/pug`
  }
});

let config = {
  context: src.root,

  entry: {},

  resolve: {
    alias: {
      Img: src.img,
      Font: src.font,
      Js: src.js,
      '@': src.root,
      $: path.resolve('node_modules','jquery/dist/jquery.min'),
      jquery: path.resolve('node_modules','jquery/dist/jquery.min')
    },

    extensions: ['.js', '.json', '.vue']
  },

  output: {
    filename: './js/[name].js',
    path: dist.root
  },

  module: {
    rules: [
      {
        exclude: [/node_modules\/(?!(swiper|dom7)\/).*/, /\.test\.jsx?$/],
        test: /\.jsx?$/,
        use: [{ loader: 'babel-loader' }],
      },
      {
        test: /\.js$/,
        include: src.root,
        loader: 'babel-loader'
      },
      {
        test: /\.styl$/,
        include: src.root,
        use: ExtractTextPlugin.extract({
          fallback: { loader: 'style-loader', options: { sourceMap: true } },
          publicPath: '../',
          use: [
            { loader: 'css-loader', options: { sourceMap: true } },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: [
                  require('autoprefixer')({browsers: ['last 10 version']})
                ]
              }
            },
            { loader: 'stylus-loader', options: { sourceMap: true } }
          ]
        })
      },
      {
        test: /\.(gif|png|jpe?g|svg|woff)$/,
        include: src.root,
        exclude: src.ico,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[path][name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        include: src.ico,
        use: ['svg-sprite-loader']
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'html/[path][name].html',
              context: src.pug
            }
          },
          'extract-loader',
          {
            loader: 'html-loader',
            options: {
              attrs: false
            }
          },
          {
            loader: 'pug-html-loader',
            options: {
              pretty: true,
              exports: false,
              doctype: 'html',
              basedir: src.pug,
              data: {
                data() {
                  return JSON.parse(fs.readFileSync(path.resolve(src.pug, 'data/global.json'), 'utf8'));
                }
              },
              filters: {
                // filter for include json data as empty string
                'json-watch': () => ''
              }
            }
          }
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          transformAssetUrls: {
            video: '',
            source: '',
            img: '',
            image: ''
          }
        }
      }
    ]
  },

  plugins: [
    new CopyWebpackPlugin([
      {
        context: src.root,
        from: path.resolve(src.img, '**/*'),
        to: dist.root
      }
    ]),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
  ]
};

module.exports = (env, argv) => {
  const platform = env.platform;
  const resource = env.resource;

  // получение имен и путей для точек входа в зависимоти от платформы
  const entries = getEntries(platform);

  // определение точкек входа в зависимости от ресурса (если не указан, то все сразу)
  switch (resource) {
    case 'js':
      config.entry[entries.app.name] = entries.app.path;
      break;
    case 'css':
      config.entry[entries.styl.name] = entries.styl.path;
      break;
    case 'pug':
      config.entry[entries.pug.name] = entries.pug.path;
      break;
    case 'ico':
      config.entry[entries.assets.name] = entries.assets.path;
      break;
    default:
      Object.keys(entries).forEach((key) => {
        const entry = entries[key];
        config.entry[entry.name] = entry.path;
      });
  }

  // определение имени извлеченного css-файла в зависимости от платформы
  config = merge(config, {
    plugins: [
      new ExtractTextPlugin(`./css/app-${platform}.css`)
    ]
  });

  // дополнения к конфигу для production
  if (argv.mode === 'production') {
    config = merge(config, {
      plugins: [
        new OptimizeCssAssetsPlugin({
          cssProcessorOptions: {
            discardComments: {
              removeAll: true
            },
            zindex: false
          }
        }),
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: JSON.stringify('production')
          }
        })
      ]
    });
  }

  // дополнения к конфигу для development
  if (argv.mode === 'development') {
    const openPage = platform === 'mobile' ? 'mobile/pages-list.html' : 'desktop/pages-list.html';

    config = merge(config, {
      devtool: 'inline-source-map',
      devServer: {
        contentBase: `../${dist.root}`,
        watchContentBase: true,
        openPage,
        open: true,
        port: platform === 'mobile' ? '3000' : '3003'
      }
    });
  }

  return config;
};
