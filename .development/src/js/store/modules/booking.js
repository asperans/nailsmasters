/* eslint-disable no-shadow */
import {
  fetchCategories,
  fetchStudios,
  fetchServices,
  fetchService,
  fetchMasters,
  fetchSchedule,
  fetchServicesStatistics,
  searchServices,
} from 'Js/api/booking';

function initialState() {
  return {
    type: 'desktop',
    queries: {
      studios: '',
      services: '',
    },
    isStandalone: false,
    isNextButtonDisabled: false,
    currentState: 'Order',
    currentStep: {
      view: 'Categories',
      index: 1,
    },
    additionalService: {
      isFetching: false,
      data: [],
    },
    currentService: {},
    openedService: '',
    currentCategory: {},
    selectedServices: [],
    servicesDictionary: {
      isFetching: false,
      data: [],
    },
    currentServices: {
      isFetching: false,
      data: [],
    },
    studios: {
      isFetching: false,
      data: {},
    },
    categories: {
      isFetching: false,
      data: [],
    },
    masters: {
      isFetching: false,
      data: [],
    },
    schedule: {
      isFetching: false,
      data: [],
    },
    servicesStatistics: {
      isFetching: false,
      data: [],
    },
    showAvailabilityPopup: false,
  };
};

const state = initialState();

const mutations = {
  SET_TYPE(state, payload) {
    state.type = payload;
  },

  SET_CURRENT_STATE(state, payload) {
    state.currentState = payload;
  },

  SET_CURRENT_STEP_VIEW(state, payload) {
    state.currentStep.view = payload;
  },

  SET_CURRENT_STEP_INDEX(state, payload) {
    state.currentStep.index = payload;
  },

  SET_SELECTED_CATEGORY(state, payload) {
    state.currentCategory = payload;
  },

  SET_CURRENT_SERVICE(state, payload) {
    state.currentService = payload;
  },

  SET_STANDALONE(state, payload) {
    state.standalone = payload;
  },

  SET_NEXTBUTTON_DISABLED(state, payload) {
    state.isNextButtonDisabled = payload;
  },

  SET_OPENED_SERVICE(state, payload) {
    state.openedService = payload;
  },

  SET_QUERY(state, payload) {
    state.queries[payload.type] = payload.value;
  },

  SET_SELECTED_SERVICES(state, payload) {
    state.selectedServices = payload;
  },

  ADD_SELECTED_SERVICE(state, payload) {
    state.selectedServices = [...state.selectedServices, payload];
  },

  DELETE_SELECTED_SERVICE(state, payload) {
    state.selectedServices =
      state.selectedServices.filter(service => service !== payload);
  },

  CLEAR_CURRENT_SERVICES(state, payload) {
    state.currentServices.data = [];
  },

  FETCHING_CATEGORIES(state) {
    state.categories.isFetching = true;
  },

  FETCHED_CATEGORIES(state, data) {
    state.categories.isFetching = false;
    state.categories.data = data;
  },

  FETCHING_STUDIOS(state) {
    state.studios.isFetching = true;
  },

  FETCHED_STUDIOS(state, data) {
    state.studios.isFetching = false;
    state.studios.data = data;
  },

  FETCHING_SERVICES(state) {
    state.currentServices.isFetching = true;
  },

  FETCHED_SERVICES(state, data) {
    state.currentServices.isFetching = false;
    state.currentServices.data = data;
  },

  FETCHING_SERVICE(state) {
    state.additionalService.isFetching = true;
  },

  FETCHED_SERVICE(state, data) {
    state.additionalService.isFetching = false;
    state.additionalService.data = data;
  },

  FETCHING_MASTERS(state) {
    state.masters.isFetching = true;
  },

  FETCHED_MASTERS(state, data) {
    state.masters.isFetching = false;
    state.masters.data = data;
  },

  FETCHING_SCHEDULE(state) {
    state.schedule.isFetching = true;
  },

  FETCHED_SCHEDULE(state, data) {
    state.schedule.isFetching = false;
    state.schedule.data = data;
  },

  FETCHING_SERVICES_STATISTICS(state) {
    state.servicesStatistics.isFetching = true;
  },

  FETCHED_SERVICES_STATISTICS(state, data) {
    state.servicesStatistics.isFetching = false;
    state.servicesStatistics.data = data;
  },

  FETCHING_SERVICES_DICTIONARY(state) {
    state.servicesDictionary.isFetching = true;
  },

  FETCHED_SERVICES_DICTIONARY(state, data) {
    state.servicesDictionary.isFetching = false;
    state.servicesDictionary.data = data;
  },

  SET_AVAILABILITY_POPUP(state, payload) {
    state.showAvailabilityPopup = payload;
  },

  RESET_BOOKING(state) {
    const s = initialState();

    Object.keys(s).forEach((key) => {
      state[key] = s[key];
    });
  },
};

const actions = {
  SET_CURRENT_STEP({ commit }, step) {
    switch (step) {
      case 'Categories':
      case 'Services':
        commit('SET_CURRENT_STEP_INDEX', 1);
        break;

      case 'Masters':
        commit('SET_CURRENT_STEP_INDEX', 2);
        break;

      case 'Contacts':
        commit('SET_CURRENT_STEP_INDEX', 3);
        break;

      case 'Success':
        commit('SET_CURRENT_STEP_INDEX', 4);
        break;

      default:
        commit('SET_CURRENT_STEP_INDEX', 1);
    }
    commit('SET_CURRENT_STEP_VIEW', step);
  },

  FETCH_CATEGORIES({ commit, rootState }) {
    commit('FETCHING_CATEGORIES');
    return fetchCategories({
      'params[studioId]': rootState.order.currentStudio.data.id,
    })
      .then(response => commit('FETCHED_CATEGORIES', response.data))
      .catch(error => Promise.reject(error));
  },

  FETCH_STUDIOS({ commit, state }, params) {
    commit('FETCHING_STUDIOS');

    return fetchStudios({
      ...params,
      ...state.selectedServices.length && {'params[services][]': state.selectedServices},
    })
      .then((response) => {
        commit('FETCHED_STUDIOS', response.data);
        return response.data;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_SERVICES({ commit }, params) {
    commit('FETCHING_SERVICES');
    return fetchServices(params)
      .then(response => commit('FETCHED_SERVICES', response.data))
      .catch(error => Promise.reject(error));
  },

  FETCH_SERVICE({ commit }, params) {
    commit('FETCHING_SERVICE');
    return fetchService(params)
      .then(response => {
        commit('FETCHED_SERVICE', response.data);
        return response.data;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_MASTERS({
    state,
    commit,
    rootState,
    dispatch,
    rootGetters,
  }, date) {
    commit('FETCHING_MASTERS');

    if (new Date(date) <= new Date()) {
      date = new Date();
      commit('SET_DATE', date, { root: true });
    }

    return fetchMasters({
      'params[studioId]': rootState.order.currentStudio.data.id,
      'params[date]': new Date(date.setHours(9)).toISOString().split('T')[0],
      'params[services][]': rootGetters.orderServices.map(item => item.id),
    })
      .then((response) => {
        commit('FETCHED_MASTERS', response.data);

        if (rootState.order.master.id) {
          dispatch('CHECK_MASTER');
        } else if (state.masters.data[0] && state.masters.data[0].active) {
          commit('SET_MASTER', state.masters.data[0]);
        } else {
          commit('SET_MASTER', {});
        }
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_SCHEDULE({ commit, rootState, rootGetters }) {
    commit('FETCHED_SCHEDULE', []);
    commit('FETCHING_SCHEDULE');
    return fetchSchedule({
      'params[studioId]': rootState.order.currentStudio.data.id,
      'params[date]':
        new Date(rootGetters.dateObject.setHours(9)).toISOString().split('T')[0],
      'params[services][]': rootGetters.orderServices.map(item => item.id),
      'params[master]': rootState.order.master.id,
    })
      .then((response) => {
        if (rootState.order.timestamp) {
          let timestamps = [];

          // Проверка наличия сохранённого таймстемпа в новых
          Object.keys(response.data).forEach((key) => {
            timestamps = [...timestamps, ...response.data[key].map(item => item.timestamp)];
          });

          if (timestamps.indexOf(rootState.order.timestamp) === -1) {
            commit('SET_TIMESTAMP', '');
          }
        }

        commit('FETCHED_SCHEDULE', response.data);

        return response.data;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_SERVICES_STATISTICS({ commit }, params) {
    commit('FETCHING_SERVICES_STATISTICS');
    return fetchServicesStatistics(params)
      .then(response => commit('FETCHED_SERVICES_STATISTICS', response.data))
      .catch(error => Promise.reject(error));
  },

  SEARCH_SERVICES({ commit }, params) {
    commit('FETCHING_SERVICES');
    return searchServices(params)
      .then((response) => {
        commit('FETCHED_SERVICES', response.data);

        return response.data;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_SERVICES_DICTIONARY({ commit, dispatch }, params) {
    commit('FETCHING_SERVICES_DICTIONARY');
    return fetchServices(params)
      .then((response) => {
        commit('FETCHED_SERVICES_DICTIONARY', response.data)
        dispatch('CHECK_SELECTED_SERVICES');
        return response.data;
      })
      .catch(error => Promise.reject(error));
  },

  CHECK_SELECTED_SERVICES({ commit, state }) {
    // Фильтрация ранее выбранных услуг на наличие их в пришедшем словаре
    if (state.selectedServices && state.servicesDictionary.data.length) {
      const filteredSelectedServices = state.selectedServices.reduce((result, id) => {
        state.servicesDictionary.data.forEach((service) => {
          if (service.id === id) {
            result.push(service.id);
          }
        });

        return result
      }, []);

      commit('SET_SELECTED_SERVICES', filteredSelectedServices);
    }
  }
};

const getters = {
  listedCategories(state, getters) {
    return state.categories.data.map((category) => {
      const result = {
        ...category,
        isActive: false,
        isOpen: false,
        amount: 0,
      };

      result.subCategories.forEach((subCategory, i) => {
        result.subCategories[i].amount = 0;
      });

      // Проверяем, есть ли в категории выбранные услуги
      // Также добавляем в счётчик количество услуг
      if (getters.orderServices.length) {
        getters.orderServices.map((service) => {
          if (service.categoryId === category.id) {
            result.isActive = true;

            if (service.subCategory) {
              result.subCategories.forEach((subCategory, i) => {
                if (subCategory.code === service.subCategory.code) {
                  result.subCategories[i].amount += 1;
                  result.amount += 1;
                }
              });
            } else {
              result.amount += 1;
            }
          }
        });
      }

      // Проверяем, открыта ли категория (нужно для десктопа)
      if (state.currentCategory.id && state.currentCategory.id === category.id) {
        result.isOpen = true;
      }

      return result;
    });
  },

  listedServices(state, getters, rootState) {
    let services = [];

    if (state.currentService.children) {
      services = state.currentService.children;
    } else {
      services = state.currentServices.data;
    }

    return services.map((service) => {
      const result = {
        ...service,
        isActive: false,
      };
      const master = rootState.order.master;

      // определяем активность
      if (getters.orderServices) {
        getters.orderServices.forEach((selectedService) => {
          if (service.children) {
            service.children.forEach((child, i) => {
              if (selectedService.id === child.id) {
                result.isActive = true;

                // добавляем поля родителя
                result.children[i].subCategory = result.subCategory;
                result.children[i].images = result.images;
              }
            });
          } else if (selectedService.id === service.id) {
            result.isActive = true;
          }
        });
      }

      return result;
    });
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
