/* eslint-disable no-shadow */
import {
  fetchStudio,
  sendOrder,
  cancelOrder,
} from 'Js/api/booking';

function initialState() {
  return  {
    date: '',
    timestamp: '',
    interval: {},
    currentStudio: {
      isFetching: false,
      data: {},
    },
    master: {},
    isCanceling: false,
    response: {
      isFetching: false,
      data: {},
    },
    cancel: false,
    contacts: {
      name: '',
      phone: '',
      email: ''
    },
    drinkAnswer: 'NOT_IMPORTANT',
    talkAnswer: 'NOT_IMPORTANT',
  }
};

const state = initialState();

const mutations = {
  SET_STUDIO(state, payload) {
    state.currentStudio.data = payload;
  },

  SET_MASTER(state, payload) {
    state.master = payload;
  },

  SET_DATE(state, payload) {
    state.date = payload;
  },

  SET_TIMESTAMP(state, payload) {
    state.timestamp = payload;
  },

  SET_INTERVAL(state, payload) {
    state.interval = payload;
  },

  SAVE_CONTACTS(state, payload) {
    state.contacts = payload;
  },

  SET_DRINK_ANSWER(state, payload) {
    state.drinkAnswer = payload;
  },

  SET_NOT_TALK(state, payload) {
    state.talkAnswer = payload;
  },

  FETCHING_DATASET_STUDIO(state) {
    state.currentStudio.isFetching = true;
  },

  FETCHED_DATASET_STUDIO(state, data) {
    state.currentStudio.isFetching = false;
    state.currentStudio.data = data;
  },

  SENDING_ORDER(state) {
    state.response.isFetching = true;
  },

  SENT_ORDER(state, data) {
    state.response.isFetching = false;
    state.response.data = data;
  },

  CANCELING_ORDER(state) {
    state.isCanceling = true;
  },

  CANCELED_ORDER(state, data) {
    state.isCanceling = false;
    state.cancel = data;
  },

  RESET_ORDER(state) {
    const s = initialState();

    Object.keys(s).forEach((key) => {
      if (key != 'contacts' &&
          key != 'currentStudio') {
        state[key] = s[key];
      }
    });
  },
};

const actions = {
  SELECT_STUDIO({ commit, rootState, dispatch }, studio) {
    commit('SET_STUDIO', studio);
    commit('SET_MASTER', {});
    commit('SET_TIMESTAMP', '');
    commit('SET_INTERVAL', '');
    if (rootState.booking.type === 'desktop') {
      dispatch('SET_CURRENT_STEP', 'Services');
    } else {
      dispatch('SET_CURRENT_STEP', 'Categories');
    }
  },

  SELECT_DATE({ commit }, date) {
    commit('SET_MASTER', {});
    commit('SET_TIMESTAMP', '');
    commit('SET_INTERVAL', '');
    commit('SET_DATE', date);
  },

  FETCH_DATASET_STUDIO({ commit, getters }, params) {
    commit('FETCHING_DATASET_STUDIO');
    return fetchStudio(params)
      .then(response => commit('FETCHED_DATASET_STUDIO', response.data))
      .catch(error => Promise.reject(error));
  },

  CHECK_MASTER({ commit, rootState, state, getters }) {
    let isAvailable = false;

    // Проверяем наличие сохранённого мастера в полученной выдаче
    rootState.booking.masters.data.forEach((master) => {
      if (master.id === state.master.id) {
        isAvailable = true;
      }
    });

    // Проверяем наличие выбранных услуг в списке компетенций
    // сохранённого мастера. Если любой мастер выбран - пропускаем проверку.
    if (state.master.services.length) {
      getters.orderServices.forEach((service) => {
        if (state.master.services.indexOf(service.id) === -1) {
          isAvailable = false;
        }
      });
    }

    // Если что-то не совпало - обнуляем мастера до первого
    if (!isAvailable) {
      commit('SET_MASTER', rootState.booking.masters.data[0]);
    }
  },

  SEND_ORDER({ commit, dispatch, state, getters }) {
    commit('SENDING_ORDER');
    return sendOrder({
      'params[studioId]': state.currentStudio.data.id,
      'params[services][]': getters.orderServices.map(item => item.id),
      'params[master]': state.master.id,
      'params[time]': state.timestamp,
      'params[drinkAnswer]': state.drinkAnswer,
      'params[talkAnswer]': state.talkAnswer,
      'params[client]': state.contacts,
    })
      .then((response) => {
        commit('SENT_ORDER', response.data);
        if (!response.data.error) {
          dispatch('SET_CURRENT_STEP', 'Success');
        }
      })
      .catch(error => Promise.reject(error));
  },

  CANCEL_ORDER({ commit, dispatch }, params) {
    commit('CANCELING_ORDER');
    return cancelOrder(params)
      .then(response => {
        if (response.data) {
          commit('CANCELED_ORDER', response.data);
        }
      })
      .catch(error => Promise.reject(error));
  },
};

const getters = {
  orderServices(state, getters, rootState) {
    if (rootState.booking.selectedServices && rootState.booking.servicesDictionary.data.length) {
      return rootState.booking.selectedServices.reduce((result, id) => {
        rootState.booking.servicesDictionary.data.forEach((service) => {
          if (service.id === id) {
            result.push(service);
          }
        });

        return result
      }, []);
    }

    return [];
  },

  dateObject(state) {
    if (state.date) {
      return new Date(state.date);
    }
    return null;
  },

  price(state, getters) {
    let total = 0;

    getters.orderServices.forEach((service) => {
      if (service.prices && service.price) {
        if (state.master.level) {
          total += service.prices[state.master.level];
        } else {
          total += service.price;
        }
      }
    });

    return {
      raw: total,
      formatted: `${total.toLocaleString()} р.`
    };
  },

  duration(state, getters) {
    let total = 0;
    const defaultSpeed = 1;

    getters.orderServices.forEach((service) => {
      if (state.master.serviceSpeed && state.master.serviceSpeed.length) {
        total += service.durations[state.master.serviceSpeed[service.id]];
      } else if (service.durations) {
        total += service.durations[defaultSpeed];
      }
    });

    const hours = Math.floor(total / 60);
    const minutes = total % 60;

    return {
      raw: total,
      formatted: `${hours ? `${hours} ч. ` : ''}${minutes ? `${minutes} мин.` : ''}`
    };
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
