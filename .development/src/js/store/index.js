/* eslint global-require: "off" */
/* eslint-disable no-shadow */
import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import booking from './modules/booking';
import order from './modules/order';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    version: '1.4.5',
    loadingQueue: [],
    error: {}
  },
  mutations: {
    // Loading from localStorage
    INITIALISE_STORE(state) {
      if (localStorage.getItem('store')) {
        const store = JSON.parse(localStorage.getItem('store'));

        if (store.version === state.version) {
          this.replaceState(Object.assign(state, store));
        }
      }
    },
    ADD_LOADING(state, payload) {
      state.loadingQueue = [...state.loadingQueue, payload];
    },
    DELETE_LOADING(state, payload) {
      state.loadingQueue =
        state.loadingQueue.filter(item => item !== payload);
    },
    ERROR(state, payload) {
      state.error = payload;
    },
    CLEAR_ERROR(state) {
      state.error = {};
    },
  },
  actions: {
    CLEAR_STORE({ commit }) {
      commit('RESET_BOOKING');
      commit('RESET_ORDER');
    },
    SET_ERROR({ commit }, params) {
      commit('ERROR', params);
    },
  },
  modules: {
    booking,
    order,
  }
});

// Saving in localStorage
store.subscribe((mutation, state) => {
  const filteredStore = {
    version: state.version,
    booking: state.booking,
    order: state.order,
  };

  localStorage.setItem('store', JSON.stringify(filteredStore));
});

export default store;
