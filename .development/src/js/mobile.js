import 'babel-polyfill';

import init from 'Js/helpers/init';
import '@fancyapps/fancybox';
import initUserCoordinates from 'Js/helpers/checkUserLocation';
import initMetricsClickTrackers from 'Js/helpers/initMetricsClickTrackers';
import LazyLoader from 'Js/helpers/LazyLoader';


// Обычные модули JS
import HeaderMenu from 'Js/mobile/widget/HeaderMenu';
import Swiper from 'Js/mobile/widget/Swiper';
import CookiePanel from 'Js/mobile/widget/CookiePanel';
import Form from 'Js/mobile/widget/Form';
import ScrollTo from 'Js/mobile/widget/common/ScrollTo';
import StudiosMetro from 'Js/mobile/widget/studios/Metro';
import StudiosSearchForm from 'Js/mobile/widget/studios/SearchForm';
import StudiosTabs from 'Js/mobile/widget/studios/Tabs';
import StudiosLoader from 'Js/mobile/widget/studios/StudiosAdd';
import BackgroundCheck from 'Js/mobile/widget/common/BackgroundCheck';
import FindNextServiceLink from 'Js/mobile/widget/services/findNextService';
import CallMeForm from 'Js/mobile/widget/CallMeForm';
import ScrollToPopular from 'Js/mobile/widget/common/ScrollToPopular';
import CareerForm from 'Js/mobile/widget/career/CareerForm';
import CareerNav from 'Js/mobile/widget/career/CareerNav';
import FranchiseForm from 'Js/mobile/widget/franchise/FranchiseForm';
import FranchiseNav from 'Js/mobile/widget/franchise/FranchiseNav';

// Компоненты Vue
import Booking from 'Js/mobile/vue/booking/Page';
import SearchServices from 'Js/mobile/vue/general/SearchServices';
import Total from 'Js/mobile/vue/booking/Total';

// Всё, что относится к Vue
import Vue from 'vue';
import Vuex from 'vuex';
import VModal from 'vue-js-modal';
import store from './store';
import SubscribeBlock from "./desktop/widget/SubscribeBlock";

// fixing IE11 "Object doesn't support property or method 'blur'" error
if (typeof SVGElement.prototype.blur === 'undefined') {
  SVGElement.prototype.blur = () => {};
}

// Активация плагинов Vue
Vue.use(Vuex);
Vue.use(VModal);

// Loading store from localStorage
store.commit('INITIALISE_STORE');

window.console.assert(true, 'GA category: ');

// Глобальные переменные
// Нужны для передачи айдишников услуг и студий при переходе на запись из вне
window.App = window.App || {};
App.dataset = App.dataset || {};

$(() => {
  $(window).on('scroll', () => {
    const $header = $('.js-header');
    const fixedClass = '_fixed';
    const floatMenuPosition = 80;
    const scrollPosition = $(window).scrollTop();

    if (scrollPosition > floatMenuPosition) {
      $header.addClass(fixedClass);
    } else {
      $header.removeClass(fixedClass);
    }
  });

  $('.js-fancybox-swiper').fancybox({
    padding: 0,
    overlay: {
      locked: false
    },
    beforeClose: function () {
      let swiperCaller = document.querySelector('[data-type="'+this.opts.fancybox+'"]');
      let swiper = swiperCaller.swiper;
      swiper.slideTo(this.index);
    }
  });

  init('.js-cookie-panel', CookiePanel, 'widget', 'CookiePanel');
  init('.js-header-menu-button', HeaderMenu, 'widget', 'HeaderMenu');
  init('.js-swiper', Swiper, 'widget', 'Swiper');
  init('.js-subscribe', SubscribeBlock, 'widget', 'SubscribeBlock');
  init('.js-call-me-form', CallMeForm, 'widget', 'CallMeForm');

  init('.js-school-form', Form, 'widget', 'Form');

  // Общие
  init('.js-scroll-to', ScrollTo, 'widget', 'ScrollTo');
  init('.js-main-logo', BackgroundCheck, 'widget', 'BackgroundCheck');
  init('.js-booking-total', Total, 'vue');
  init('.js-scroll-to-popular', ScrollToPopular, 'widget', 'ScrollToPopular');

  // Студии
  init('.js-studios-tabs', StudiosTabs, 'widget', 'StudiosTabs');
  init('.js-studios-search-form', StudiosSearchForm, 'widget', 'StudiosSearchForm');
  init('.js-studios-metro', StudiosMetro, 'widget', 'StudiosMetro');
  init('.js-studios-add', StudiosLoader, 'widget', 'StudiosAdd');
  init('.js-find-next-service-link', FindNextServiceLink, 'widget', 'findNextService');
  init('.js-career-form', CareerForm, 'widget', 'CareerForm');
  init('.js-scroll-to-career-form', CareerNav, 'widget', 'CareerNav');
  // Запись
  init('.js-booking-page-mobile', Booking, 'vue');

  // Услуги
  init('.js-search-services', SearchServices, 'vue');

  // Франшиза
  init('.js-scroll-to-franchise-form', FranchiseNav, 'widget', 'FranchiseNav');
  init('.js-franchise-form', FranchiseForm, 'widget', 'FranchiseForm');

  initUserCoordinates();
  initMetricsClickTrackers();
  new LazyLoader();
});
