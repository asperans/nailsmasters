import fetcher from 'Js/helpers/fetcher';

export function fetchCategories(params = {}) {
  return fetcher({
    url: '/api/booking/categories/',
    method: 'get',
    params,
  });
}

export function fetchStudios(params = {}) {
  return fetcher({
    url: '/api/booking/studios/',
    method: 'get',
    params,
  });
}

export function fetchStudio(params = {}) {
  return fetcher({
    url: '/api/booking/studio/',
    method: 'get',
    params,
  });
}

export function fetchServices(params = {}) {
  return fetcher({
    url: '/api/booking/services/',
    method: 'get',
    params,
  });
}

export function fetchService(params = {}) {
  return fetcher({
    url: '/api/booking/service/',
    method: 'get',
    params,
  });
}

export function fetchMasters(params = {}) {
  return fetcher({
    url: '/api/booking/masters/',
    method: 'get',
    params,
  });
}

export function fetchSchedule(params = {}) {
  return fetcher({
    url: '/api/booking/schedule/',
    method: 'get',
    params,
  });
}

export function sendOrder(params = {}) {
  return fetcher({
    url: '/api/booking/order/',
    method: 'post',
    params
  });
}

// Отмена записи
// Параметры
// params[id] - id записи. Если записей несколько, то их id указываются через запятую
export function cancelOrder(params = {}) {
  return fetcher({
    url: '/api/booking/cancel/',
    method: 'get',
    params
  });
}

// Информация о количестве услуг в студии
// Параметры:
// params[studioId] - id студии
export function fetchServicesStatistics(params = {}) {
  return fetcher({
    url: '/api/service/statistics/',
    method: 'get',
    params
  });
}

// Поиск по услугам
// Параметры:
// params[q] - поисковая строка
// params[studioId] - id студии
// params[page]=booking - режим поиска. Остаётся без измений для записи
// view - режим записи. desktop или пустое значение
export function searchServices(params = {}) {
  return fetcher({
    url: '/api/service/search/',
    method: 'get',
    params
  });
}
