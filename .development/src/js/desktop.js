import 'babel-polyfill';

import init from 'Js/helpers/init';
import 'Js/helpers/jquery.nav';
import VueDragscroll from 'vue-dragscroll';
import '@fancyapps/fancybox';
import VTooltip from 'v-tooltip';
import initUserCoordinates from 'Js/helpers/checkUserLocation';
import initMetricsClickTrackers from 'Js/helpers/initMetricsClickTrackers';

// Обычные модули JS
import Swiper from 'Js/desktop/widget/Swiper';
import StudiosIndex from 'Js/desktop/widget/StudiosIndex';
import SubscribeBlock from 'Js/desktop/widget/SubscribeBlock';
import ServiceDetail from 'Js/desktop/widget/services/ServiceDetail';
import ServiceDetailMenu from 'Js/desktop/widget/services/ServiceDetailMenu';
import StudioDetailNav from 'Js/desktop/widget/studios/StudioDetailNav';
import CallMeForm from 'Js/desktop/widget/CallMeForm';
import ServicesMenu from 'Js/desktop/widget/services/ServicesMenu';
import FranchiseNav from 'Js/desktop/widget/franchise/FranchiseNav';
import FranchiseMenu from 'Js/desktop/widget/franchise/FranchiseMenu';
import FranchiseForm from 'Js/desktop/widget/franchise/FranchiseForm';
import CareerMenu from 'Js/desktop/widget/career/CareerMenu';
import CareerForm from 'Js/desktop/widget/career/CareerForm';
import CareerNav from 'Js/desktop/widget/career/CareerNav';

// Компоненты Vue
import Studios from 'Js/desktop/vue/booking/Studios';
import Booking from 'Js/desktop/vue/booking/Page';
import GallerySlider from 'Js/desktop/vue/general/GallerySlider';
import Services from 'Js/desktop/vue/booking/Services';
import SearchServices from 'Js/desktop/vue/general/SearchServices';

// Всё, что относится к Vue
import Vue from 'vue';
import Vuex from 'vuex';
import VModal from 'vue-js-modal';
import store from './store';
import CourseEnrollForm from "./desktop/widget/school/CourseEnrollForm";

// fixing IE11 "Object doesn't support property or method 'blur'" error
if (typeof SVGElement.prototype.blur === 'undefined') {
  SVGElement.prototype.blur = () => {};
}

// Активация плагинов Vue
Vue.use(Vuex);
Vue.use(VModal);
Vue.use(VueDragscroll);
Vue.use(VTooltip, {
  disposeTimeout: 0,
});

// Loading store from localStorage
store.commit('INITIALISE_STORE');

$(() => {
  init('.js-swiper', Swiper, 'widget', 'Swiper');
  init('.js-studios-index', StudiosIndex, 'widget', 'StudiosIndex');
  init('.js-subscribe', SubscribeBlock, 'widget', 'SubscribeBlock');
  init('.js-call-me-form', CallMeForm, 'widget', 'CallMeForm');

  // Услуги
  $('.js-one-page-nav').onePageNav({
    currentClass: '_active',
    changeHash: false,
    scrollSpeed: 500,
    scrollOffset: 80,
    filter: ':not(.external)'
  });

  // Для того, чтобы при закрытии fancybox не происходило скролла к элементу страницы,
  // который указали в hash
  const removeHash = function () {
    const uri = window.location.toString();
    if (uri.indexOf('#') > 0) {
      const clean_uri = uri.substring(0, uri.indexOf('#'));
      window.history.replaceState({}, document.title, clean_uri);
    }
  };
  document.addEventListener('scrollStop', removeHash, false);

  init('.js-service-detail-page', ServiceDetail, 'widget', 'ServiceDetail');
  init('.js-service-detail-menu', ServiceDetailMenu, 'widget', 'ServiceDetailMenu');
  init('.js-search-services', SearchServices, 'vue');
  init('.js-services-menu', ServicesMenu, 'widget', 'ServicesMenu');
  // Страница студии
  init('.js-gallery-slider', GallerySlider, 'vue');
  init('.js-studio-services', Services, 'vue');
  init('.js-studio-detail-nav', StudioDetailNav, 'widget', 'StudioDetailNav');
  // Студии
  init('.js-studios-page-desktop', Studios, 'vue');
  // Запись
  init('.js-booking-page-desktop', Booking, 'vue');
  // Франшиза
  init('.js-scroll-to-franchise-form', FranchiseNav, 'widget', 'FranchiseNav');
  init('.js-franchise-menu', FranchiseMenu, 'widget', 'FranchiseMenu');
  init('.js-franchise-form', FranchiseForm, 'widget', 'FranchiseForm');

  init('.js-career-menu', CareerMenu, 'widget', 'CareerMenu');
  init('.js-career-form', CareerForm, 'widget', 'CareerForm');
  init('.js-scroll-to-career-form', CareerNav, 'widget', 'CareerNav');
  init('.js-course-enroll-form', CourseEnrollForm, 'widget', 'CourseEnrollForm');


  initMetricsClickTrackers();
  // Геолокация
  initUserCoordinates();
});
