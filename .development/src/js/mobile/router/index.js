import Vue from 'vue';
import Router from 'vue-router';
import Order from 'Js/mobile/vue/booking/Order';
import Studios from 'Js/mobile/vue/booking/Studios';
import store from 'Js/store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Order',
      component: Order,
    },
    {
      path: '/studios/',
      name: 'Studios',
      component: Studios,
    }
  ],
});

router.replace('/')

export default router;
