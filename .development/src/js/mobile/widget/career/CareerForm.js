import PhoneMasker from 'Js/helpers/PhoneMasker';

export default class {
  constructor($c) {
    let self = this;
    self.$c = $c;

    self.button = $c.find('.js-submit-button');
    self.phoneInput = $c.find('.js-phone-required');
    self.nameInput = $c.find('.js-name-required');

    new PhoneMasker({
      className: '.js-phone-required',
      onComplete: () => {
        if (self.nameInput.val().length > 0) {
          self.button.attr('disabled', false);
          self.button.removeClass('_active');
        }
      },
      onIncomplete: () => {
        self.button.attr('disabled', true);
        self.button.addClass('_active');
      }
    });

    self.nameInput.on('keyup', (event) => {
      event.preventDefault();

      self.validatePhone();

      if ($(event.target)[0].value !== '') {
        self.button.attr('disabled', false);
        self.button.removeClass('_active');
      } else {
        self.button.attr('disabled', true);
        self.button.addClass('_active');
      }
    });

    self.validatePhone = () => {
      let intPhone = self.phoneInput.val().replace(/[^0-9]/g,'');

      if (intPhone.length !== 11) {
        self.button.attr('disabled', true);
        self.button.addClass('_active');
        return false;
      }
    };

    $c.on('submit', (event) => {
      event.preventDefault();

      self.validatePhone();

      const form = $(event.target);

      const $formData = form.serialize();
      const $formActionUrl = form.attr('action');
      const $formMethod = form.attr('method');

      $.ajax({
        url: $formActionUrl,
        data: $formData,
        dataType: 'json',
        method: $formMethod,
        success(data) {
          if (data) {
            $('.js-career-form-success-message').show();
            $('.js-career-form').hide();
          }
        }
      });

      return false;
    });
  }
}
