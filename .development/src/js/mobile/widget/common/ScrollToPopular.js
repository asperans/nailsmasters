export default class {
  constructor($c) {
    this.$c = $c;

    $c.on('click', (event) => {
      event.preventDefault();

      const popularTop = $('.js-popular-services').offset().top - 150;
      $('body,html').animate({scrollTop: popularTop}, 600);
    });
  }
}
