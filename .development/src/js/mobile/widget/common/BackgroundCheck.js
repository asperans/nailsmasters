export default class {
  constructor($logo) {
    document.onreadystatechange = () => {
      this.image = document.querySelector('.js-background-image');

      if (this.image) {
        this.getImageBrightness((brightness) => {
          if (brightness < 100) {
            $logo[0].classList.add('_whiten');
          } else {
            $logo[0].classList.remove('_whiten');
          }
        });
      }
    };
  }

  getImageBrightness(callback) {
    const img = document.createElement('img');
    let colorSum = 0;

    img.src = this.image.src;
    img.style.display = 'none';

    document.body.appendChild(img);

    img.onload = function load() {
      const canvas = document.createElement('canvas');

      canvas.width = this.width;
      canvas.height = this.height;

      const ctx = canvas.getContext('2d');

      ctx.drawImage(this, 0, 0);

      const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      const { data } = imageData;

      let r = 0;
      let g = 0;
      let b = 0;
      let avg = 0;

      for (let x = 0, len = data.length; x < len; x += 4) {
        r = data[x];
        g = data[x + 1];
        b = data[x + 2];
        avg = Math.floor((r + g + b) / 3);
        colorSum += avg;
      }

      const brightness = Math.floor(colorSum / (this.width * this.height));

      callback(brightness);
    };
  }
}
