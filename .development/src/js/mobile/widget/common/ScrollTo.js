export default class {
  constructor($c) {
    this.$c = $c;

    $c.on('click', (event) => {
      event.preventDefault();

      const $target = $(this.$c.attr('href'));

      if ($target.length > 0) {
        const $header = $('.js-header');
        let offset = 60;

        if ($header.length > 0 && !$header.hasClass('_fixed')) {
          offset += $header.outerHeight();
        }

        $('body,html').animate({ scrollTop: $target.offset().top - offset }, 500);
      }
    });
  }
}
