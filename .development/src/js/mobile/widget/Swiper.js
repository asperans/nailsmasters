import Swiper from 'swiper';

export default class {
  constructor($c) {
    this.el = $c[0] ? $c[0] : $c;

    const type = this.el.getAttribute('data-type');

    this.swiper = new Swiper(this.el, this.getParameters(type));
  }

  getParameters(type) {
    const parameters = {
      pagination: {
        el: this.el.querySelector('.swiper-pagination'),
        clickable: true,
        dynamicBullets: true,
      },
      noSwipingClass: 'js-slider-no-swiping',
    };

    switch (type) {
      case 'offers':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 38,
        });
        break;

      case 'popular':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 22,
        });
        break;

      case 'review':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 22,
        });
        break;

      case 'work':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 22,
        });
        break;

      case 'index-gallery':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 22,
        });
        break;

      case 'service-step':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 14,
        });
        break;

      case 'service-testimonials':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 20,
        });
        break;

      case 'studio-detail':
        Object.assign(parameters, {
          navigation: {
            nextEl: this.el.querySelector('.swiper-button-next'),
            prevEl: this.el.querySelector('.swiper-button-prev'),
          },
        });
        break;

      case 'calendar':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 4,
        });
        break;

      case 'schedule-type-1':
        Object.assign(parameters, {
          slidesPerView: 1,
          autoHeight: true,
          navigation: {
            nextEl: this.el.querySelector('.swiper-button-next'),
            prevEl: this.el.querySelector('.swiper-button-prev'),
          },
        });
        break;

      case 'schedule-type-2':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          autoHeight: true,
          navigation: {
            nextEl: this.el.querySelector('.swiper-button-next'),
            prevEl: this.el.querySelector('.swiper-button-prev'),
          },
        });
        break;

      default:
    }

    return parameters;
  }
}
