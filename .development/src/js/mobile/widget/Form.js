export default class {
  constructor($c) {
    const el = $c[0] ? $c[0] : $c;

    const $successBlocks = $(el).find('.js-success');
    const $contentBlocks = $(el).find('.js-form');

    this.$form = el.querySelector('.js-form');

    $(this.$form).on('submit', (event) => {
      event.preventDefault();

      const form = $(event.target);
      const $formData = form.serialize();

      $.ajax({
        url: form.attr('action'),
        data: $formData,
        dataType: 'json',
        method: form.attr('method'),
        success(result) {
          if (result) {
            $successBlocks.show();
            $contentBlocks.hide();
          } else {
            alert('Произошла непредвиденная ошибка');
          }
        },
        error(result) {
          alert(result.responseJSON.error.message);
        }
      });

      return false;
    });
  }
}
