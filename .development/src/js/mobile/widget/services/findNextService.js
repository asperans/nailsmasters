export default class  {
  constructor($c) {

    this.$c = $c;

    $c.click((event) => {
      event.preventDefault();
      let block = $('.js-services-block').offset().top;

      $('html, body').animate({scrollTop: block - 120}, 500);
    });
  }
}
