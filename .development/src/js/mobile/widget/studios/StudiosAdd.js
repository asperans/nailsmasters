import LazyLoader from 'Js/helpers/LazyLoader';

export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.button', () => {
      const targetContainer = $('.js-studios-container');
      const url = {
        url: this.$c.attr('data-url')
      };
      const n = $('.js-studios-add').attr('data-n');

      if (typeof url !== 'undefined') {
        $.ajax({
          url: url.url,
          dataType: 'html',
          data: {
            CUR_PAGE: n
          },
          type: 'GET'
        }).done((data) => {
          $('.js-studios-add').remove();
          targetContainer.append(data);
          new LazyLoader();
        });
      }
    });
  }
}
