import LazyLoader from 'Js/helpers/LazyLoader';

export default class {
  constructor($c) {
    this.$c = $c;
    this.$field = $c.find('.js-search-field');

    this.query = '';
    this.searchTimeout = null;
    this.isFirstSearch = true;
    this.searchInProgress = false;

    $c.on('submit', (event) => {
      event.preventDefault();

      this.formSubmit($(event.target));
    });

    this.$field.on('keyup', (event) => {
      const query = event.target.value;

      if (query !== this.query) {
        this.query = query;

        clearTimeout(this.searchTimeout);

        if (query.length > 2) {
          this.searchTimeout = setTimeout(
            $.proxy(this.doSearch, this),
            300
          );
        }
      }

      if (query === '') {
        this.query = '';

        // Чтобы не было ложного поиска при ввооде одной буквы и последующем её удалении
        if (!this.isFirstSearch) {
          this.doSearch(this.$c);
        }
      }
    });
  }

  formSubmit(form) {
    this.doSearch(form);
  }

  doSearch(form = this.$c) {
    if (this.searchInProgress) {
      return;
    }

    this.isFirstSearch = false;

    $.ajax({
      url: form.attr('action'),
      data: form.serialize(),
      // dataType: 'json',
      method: 'GET'
    }).done((data) => {
      $('.js-studios-container').html(data);

      this.searchInProgress = false;
      new LazyLoader();
    });

    this.searchInProgress = true;
  }
}
