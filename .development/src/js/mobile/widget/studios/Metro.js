import 'jquery.panzoom';

export default class {
  constructor($c) {
    this.$c = $c;

    $c.find('.js-panzoom').panzoom({
      minScale: 0.4,
      contain: 'invert',
    });

    $c.find('.js-metro-map-link').on('touchstart', (event) => {
      event.stopImmediatePropagation();
    });
  }
}
