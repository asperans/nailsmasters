export default class {
  constructor($c) {
    this.$c = $c;
    this.$tabs = this.$c.find('.js-tab');
    this.$views = this.$c.find('.js-studios-view');

    this.$tabs.on('click', (event) => {
      const $el = $(event.target);

      this.$tabs.removeClass('_active');
      $el.addClass('_active');

      this.$views.removeClass('_active');
      this.$c.find($el.data('container')).addClass('_active');
    });
  }
}
