import yaMetricsSendEvent from 'Js/helpers/yaMetricsSendEvent';
import gaSendEvent from 'Js/helpers/gaSendEvent';
import PhoneMasker from 'Js/helpers/PhoneMasker';

export default class {
  constructor($c) {
    let self = this;
    self.$c = $c;

    self.button = $c.find('.js-call-me-button');
    self.phoneInput = $c.find('.js-phone-required');
    self.nameInput = $c.find('.js-name-required');

    new PhoneMasker({
      className: '.js-phone-required',
      onComplete: () => {
        $('.js-call-me-error-message').hide();
        self.phoneInput.next().hide();
        if (self.nameInput.val().length > 0) {
          self.button.attr('disabled', false);
          self.button.removeClass('_active');
        }
      },
      onIncomplete: () => {
        if (!self.phoneInput.val().length) {
          self.phoneInput.next().html('Поле "Телефон" не может быть пустым!');
        } else {
          self.phoneInput.next().html('Введите номер телефона полностью');
        }
        self.phoneInput.next().show();
        self.button.attr('disabled', true);
        self.button.addClass('_active');
      }
    });

    self.nameInput.on('keyup', (event) => {
      event.preventDefault();

      if ($(event.target)[0].value !== '') {
        $('.js-call-me-error-message').hide();
        self.nameInput.next().hide();
        if (self.phoneInput.val().length) {
          self.button.attr('disabled', false);
          self.button.removeClass('_active');
        }
      } else {
        self.nameInput.next().show();
        self.button.attr('disabled', true);
        self.button.addClass('_active');
      }
    });

    self.validatePhone = () => {
      let intPhone = self.phoneInput.val().replace(/[^0-9]/g,'');

      if (intPhone.length !== 11) {
        self.phoneInput.next().show();
        self.button.attr('disabled', true);
        self.button.addClass('_active');
        return false;
      }
    };

    $c.on('submit', (event) => {
      event.preventDefault();

      self.validatePhone();

      const form = $(event.target);

      const $formData = form.serialize();
      const $formActionUrl = form.attr('action');
      const $formMethod = form.attr('method');

      $.ajax({
        url: $formActionUrl,
        data: $formData,
        dataType: 'json',
        method: $formMethod,
        success(data) {
          if (data) {
            $('.js-call-me-success-message').show();
            $('.js-call-me-form').hide();

            yaMetricsSendEvent('call-me');
            gaSendEvent('call-me');
          }
        },
        error(error) {
          if (error.responseJSON.error.message === 'Phone should be not empty') {
            self.phoneInput.next().show();
          }
          else if (error.responseJSON.error.message === 'Name should be not empty') {
            self.nameInput.next().show();
          }
          else {
            $('.js-call-me-error-message').show();
          }
        }
      });

      return false;
    });
  }
}
