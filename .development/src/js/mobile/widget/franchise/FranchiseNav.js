export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.js-scroll-to-franchise-form', () => {
      // скролл к форме Оставить заявку на Франшизу
      let block = $('#franchise-form').offset().top;

      $('html, body').animate({scrollTop: block - 140}, 500);
    });
  }
}
