import * as Cookies from 'js-cookie';

export default class {
  constructor($c) {
    this.$c = $c;

    const cookieName = $c.data('cookie-name');

    $c.find('.js-close').on('click', (event) => {
      event.preventDefault();

      Cookies.set(cookieName, 'Y', { expires: 365 });

      this.$c.addClass('_hidden');
    });
  }
}
