export default class {
  constructor($c) {
    this.$header = $('.js-header');

    $c.on('click', (event) => {
      event.preventDefault();

      this.$header.toggleClass('_menu-open');
    });

    $(document).on('click touchstart', (event) => {
      if (!$(event.target).closest('.js-header-menu, .js-header-menu-button').length) {
        this.$header.removeClass('_menu-open');
      } else {
        event.stopPropagation();
      }
    });
  }
}
