export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.js-scroll-to-courses', () => {
        // скролл к списку курсов
        let block = $('#courses').offset().top;

        $('html, body').animate({scrollTop: block - 65}, 500);
    });
  }
}
