export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.js-scroll-to-form', () => {
        // скролл до формы Записаться на курс
        let block = $('#enroll').offset().top;

        $('html, body').animate({scrollTop: block - 75}, 500);
    });
  }
}
