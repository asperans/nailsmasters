export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.js-scroll-to-career-form', () => {
      // скролл к форме Оставить заявку
      let block = $('#career-form').offset().top;

      $('html, body').animate({ scrollTop: block - 45 }, 500);
    });
  }
}
