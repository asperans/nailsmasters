export default class {
  constructor($c) {
    const el = $c[0] ? $c[0] : $c;

    this.tabHeads = el.querySelectorAll('.js-studios-index-tab-head');
    this.tabDescriptions = el.querySelectorAll('.js-studios-index-tab-description');
    this.tabPhotos = el.querySelectorAll('.js-studios-index-tab-photo');

    this.toArray(this.tabHeads).forEach((head) => {
      const dataTab = head.getAttribute('data-tab');

      head.addEventListener('click', () => this.handleTabClick(dataTab));
    });
  }

  handleTabClick(dataTab) {
    this.toggleTab(this.tabDescriptions, dataTab);
    this.toggleTab(this.tabPhotos, dataTab);
    this.toggleTab(this.tabHeads, dataTab);
  }

  toggleTab(group, dataTab) {
    this.toArray(group).forEach((item) => {
      const itemDataTab = item.getAttribute('data-tab');

      if (itemDataTab === dataTab) {
        item.classList.add('_active');
      } else {
        item.classList.remove('_active');
      }
    });
  }

  toArray(arrLike) {
    return [].slice.call(arrLike);
  }
}
