
export default class {
  constructor($c) {
    this.$c = $c;

    $(window).on('scroll', () => {
      const fixedClass = '_fixed';
      const menuTop = 460;
      const menuBottom = menuTop + parseInt($('.js-text-block').height() - this.$c.height());
      const scrollPosition = $(window).scrollTop();

      if (scrollPosition < menuTop) {
        this.$c.removeClass(fixedClass);
        this.$c.css('bottom', 'auto');
      } else if (scrollPosition >  menuBottom) {
        this.$c.removeClass(fixedClass);
        this.$c.css('bottom', '390px');
      } else {
        this.$c.addClass(fixedClass);
        this.$c.css('bottom', 'auto');
      }
    });
  }
}
