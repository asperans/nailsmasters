import Swiper from 'Js/desktop/widget/Swiper';

export default class {
  constructor($c) {
    this.$c = $c;
    this.activeSlide = 0;

    self.swiper = $c.find('.js-images-swiper');
    self.current = $c.find('.js-current');
    this.swiper = new Swiper(self.swiper);
    this.slides = document.querySelectorAll('a.js-upper-slider');
    this.smallSlides = document.querySelectorAll('div.js-swiper-slide');
    this.length = this.slides.length;

    [...this.smallSlides].forEach((item, index) => {
      item.addEventListener('click', () => this.clickSlide(index));
    });

    $c.find('.js-next').on('click', (event) => {
      event.preventDefault();
      let nextSlide = this.activeSlide + 1;

      if (nextSlide >= 0 && nextSlide < this.length) {
        this.swiper.swiper.slideTo(nextSlide);
        this.activeSlide = nextSlide;
        this.toggleActive(this.slides, this.activeSlide);
        this.toggleActive(this.smallSlides, this.activeSlide);
      }
    });

    $c.find('.js-prev').on('click', (event) => {
      event.preventDefault();
      let prevSlide = this.activeSlide - 1;

      if (prevSlide >= 0 && prevSlide < this.length) {
        this.swiper.swiper.slideTo(prevSlide);
        this.activeSlide = prevSlide;
        this.toggleActive(this.slides, this.activeSlide);
        this.toggleActive(this.smallSlides, this.activeSlide);
      }
    });
  }

  clickSlide(index) {
    this.toggleActive(this.slides, index);
    this.toggleActive(this.smallSlides, index);
  }

  toggleActive(nodeGroup, i) {
    [...nodeGroup].forEach((node, index) => {
      if (i !== index) {
        node.classList.remove('_active');
      } else {
        node.classList.add('_active');
      }
    });

    if (i < 9) {
      self.current.html(`0${i + 1} /`);
    } else {
      self.current.html(`${i + 1} /`);
    }
  }
}
