import yaMetricsSendEvent from "../../helpers/yaMetricsSendEvent";
import gaSendEvent from "../../helpers/gaSendEvent";

export default class {
  constructor($c) {
    const el = $c[0] ? $c[0] : $c;

    const $successBlocks = $(el).find('.js-success');
    const $contentBlocks = $(el).find('.js-content');

    this.$form = el.querySelector('.js-form');

    $(this.$form).on('submit', (event) => {
      event.preventDefault();

      const form = $(event.target);

      yaMetricsSendEvent('main-subscribe');
      gaSendEvent('main-subscribe');

      $.ajax({
        url: form.attr('action'),
        data: {
          params: {
            email: form.find('input[name="email"]').val()
          }
        },
        dataType: 'json',
        method: form.attr('method'),
        success(result) {
          if (result) {
            $successBlocks.show();
            $contentBlocks.hide();
          } else {
            alert('Произошла непредвиденная ошибка');
          }
        },
        error(result) {
          alert(result.responseJSON.error.message);
        }
      });

      return false;
    });
  }
}
