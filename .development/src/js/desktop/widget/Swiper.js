import Swiper from 'swiper';

export default class {
  constructor($c) {
    this.el = $c[0] ? $c[0] : $c;

    const type = this.el.getAttribute('data-type');

    this.swiper = new Swiper(this.el, this.getParameters(type));
  }

  getParameters(type) {
    const parameters = {
      pagination: {
        el: this.el.querySelector('.js-slider-pagination'),
        bulletClass: 'bullets__item',
        bulletActiveClass: '_active',
        clickable: true,
      },
      noSwipingClass: 'js-slider-no-swiping',
    };

    switch (type) {
      case 'gallery-slider':
        Object.assign(parameters, {
          slidesPerView: 'auto',
        });
        break;

      case 'calendar':
        Object.assign(parameters, {
          slidesPerView: 1,
          navigation: {
            prevEl: this.el.parentNode.querySelector('.booking-calendar__prev'),
            nextEl: this.el.parentNode.querySelector('.booking-calendar__next'),
          },
        });
        break;

      case 'title-popular':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 20,
          navigation: {
            prevEl: this.el.parentNode.querySelector('.title-popular-services__left'),
            nextEl: this.el.parentNode.querySelector('.title-popular-services__right'),
          },
        });
        break;

      case 'index-portfolio':
        Object.assign(parameters, {
          slidesPerView: 1,
          pagination: {
            el: document.querySelector('.js-index-portfolio-pagination'),
            formatFractionCurrent(number) {
              return number < 10 ? `0${number}` : number;
            },
            renderFraction(currentClass, totalClass) {
              return `<span class="${currentClass}"></span>`;
            },
            type: 'fraction',
          },
          navigation: {
            prevEl: document.querySelector('.js-index-portfolio-slider-left'),
            nextEl: document.querySelector('.js-index-portfolio-slider-right'),
          },
        });
        break;

      case 'index-gallery':
        Object.assign(parameters, {
          slidesPerView: 1,
          pagination: {
            el: document.querySelector('.js-index-gallery-pagination'),
            formatFractionCurrent(number) {
              return number < 10 ? `0${number}` : number;
            },
            renderFraction(currentClass, totalClass) {
              return `<span class="${currentClass}"></span>`;
            },
            type: 'fraction',
          },
          navigation: {
            prevEl: document.querySelector('.js-index-gallery-slider-left'),
            nextEl: document.querySelector('.js-index-gallery-slider-right'),
          },
        });
        break;

      default:
    }

    return parameters;
  }
}
