export default class {
  constructor($c) {
    this.$c = $c;

    $(document).on('click', '.js-scroll-studios', (event) => {
      event.preventDefault();

      let studiosTop = $('.booking-studios-list').offset().top - 60;
      $('body,html').animate({scrollTop: studiosTop}, 600);
    });
  }
}
