export default class {
  constructor($c) {
    this.$c = $c;

    this.sections = {};
    this.setSections();

    $(window).on('scroll', () => {
      const fixedClass = '_fixed';
      const menuTop = 628;
      const scrollPosition = $(window).scrollTop();

      if (scrollPosition < menuTop) {
        this.$c.css('top', '0px');
        this.$c.removeClass(fixedClass);
      } else {
        this.$c.css('top', '80px');
        this.$c.addClass(fixedClass);
      }

      const sectionPosition = this.getSection(scrollPosition);

      if (sectionPosition !== null) {
        $('.js-studio-detail-fixed-link').each(function() {
          const linkHref = $(this).attr('data-link');

          if (linkHref === sectionPosition) {
            if (!$(this).parent().hasClass('_active')) {
              $('.js-studio-detail-fixed-link').parent().removeClass('_active');
              $(this).parent().addClass('_active');
            }
          }

        });
      }
    });

    $(document).on('click', '.js-studio-detail-fixed-link', (event) => {
      event.preventDefault();

      const sectionTop = $('#'+event.target.getAttribute('data-link')).offset().top - 150;
      $('body,html').animate({scrollTop: sectionTop}, 600);
    });
  }

  setSections() {
    const self = this;

    $('.js-studio-detail-section').each(function() {
      const linkHref = $(this).attr('id');

      self.sections[linkHref] = Math.round($(this).offset().top);
    });
  }

  getSection(scrollPosition) {
    let returnValue = null;
    const windowHeight = Math.round($(window).height() * 0.5);

    for (let section in this.sections) {
      if ((this.sections[section] - windowHeight) < scrollPosition) {
        returnValue = section;
      }
    }

    return returnValue;
  }
}
