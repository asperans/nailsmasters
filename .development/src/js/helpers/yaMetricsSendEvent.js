/* eslint-disable no-console */

import * as Cookies from 'js-cookie';

export default function (goal, params) {
  const debugMode = Cookies.get('debug');

  if (debugMode !== 'undefined') {
    console.log(`Metrika goal: ${goal}`);

    if (params) {
      console.log(params);
    }
  }

  if (typeof window.yaCounter50248446 === 'undefined') {
    return;
  }

  if (params) {
    window.yaCounter50248446.reachGoal(goal, params);
  } else {
    window.yaCounter50248446.reachGoal(goal);
  }
}
