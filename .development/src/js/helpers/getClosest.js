export default function (stack, needle) {
  if (!document.documentElement.contains(stack)) {
    return null;
  }
  do {
    if (stack.matches(needle)) {
      return stack;
    }

    stack = stack.parentElement || stack.parentNode;
  } while (stack !== null && stack.nodeType === 1);

  return null;
}
