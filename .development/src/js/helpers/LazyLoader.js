export default class {
  constructor() {
    const idlers = document.querySelectorAll('.js-lazy-load');

    [...idlers].forEach((idler) => {
      switch (idler.tagName) {
        case 'IMG':
          this.changeImgSrc(idler);
          break;
        default:
      }
    });
  }

  changeImgSrc(idler) {
    const src = idler.getAttribute('data-src');
    const srcset = idler.getAttribute('data-srcset');

    if (src) {
      idler.src = src;
    }

    if (srcset) {
      idler.srcset = srcset;
    }

    idler.classList.remove('js-lazy-load');
  }
}
