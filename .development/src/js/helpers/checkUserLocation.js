import * as Cookies from 'js-cookie';

export default function () {
  const cookieName = 'UserPosition';

  function isUserPosExist(name) {
    if (Cookies.get(name)) {
      return true;
    }

    return false;
  }

  function loadNearestStudio() {
    const placeholder = $('.js-nearest-studio-placeholder');

    if (placeholder.length > 0) {
      $.ajax({
        url: '/local/ajax/common/studios/nearest-studio.php',
        method: 'GET'
      }).done((data) => {
        $('.js-nearest-studio-placeholder').replaceWith(data);
      });
      /* $.ajax({
          url: '/local/ajax/common/studios/nearest_studio.php',
          success: function success(data) {
              if (data) {
                  $('.js-nearest-studio-placeholder').replaceWith(data);
              }
          }
      }); */
    }
  }

  function savePosition(position, notBrower) {
    const date = new Date();
    const minutes = 5;
    const value = `${position.coords.latitude},${position.coords.longitude}`;
    const geoApiCaught = notBrower === true ? 'N' : 'Y';

    date.setTime(date.getTime() + (minutes * 60 * 1000));

    Cookies.set(cookieName, value, { expires: date });
    $.ajax({
      url: '/bitrix/ajax/common/save_coordinates.php',
      data: {
        coordinates: value,
        geoApiCaught,
      }
    });

    loadNearestStudio();
  }

  function loadPositionFromYandex() {
    ymaps.ready(() => {
      ymaps.geolocation.get({
        provider: 'yandex',
        mapStateAutoApply: true,
      }).then((result) => {
        const { position } = result.geoObjects;

        if (position && position !== 'undefined') {
          const pos = {
            coords: {
              latitude: position[0],
              longitude: position[1]
            }
          };

          savePosition(pos);
        }
      });
    });
  }

  if (!isUserPosExist(cookieName)) {
    if (typeof navigator.geolocation !== 'undefined' && window.location.protocol === 'https:') {
      navigator.geolocation.getCurrentPosition(savePosition);
    } else if (typeof ymaps === 'undefined') {
      $.getScript('//api-maps.yandex.ru/2.1/?lang=ru-RU', $.proxy(loadPositionFromYandex, this));
    } else {
      loadPositionFromYandex();
    }
  }
}
