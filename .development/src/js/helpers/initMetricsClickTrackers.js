
import yaMetricsSendEvent from 'Js/helpers/yaMetricsSendEvent';
import gaSandEvent from 'Js/helpers/gaSendEvent';
// @todo Доработать отправку при клике на ссылки и при отправке форм
export default function () {
  // eslint-disable-next-line func-names
  $('.js-ya-goal').click(function (ev) {
    const $el = $(this);
    const isLink = $el.is('a') && $el.attr('target') !== '_blank';

    if (isLink) {
      ev.preventDefault();
    }

    const goal = $(this).data('goal');

    if (goal) {
      const params = $(this).data('params');

      yaMetricsSendEvent(goal, params);

      // @todo Только на время проведения A/B-теста
      if (goal === 'main-hero-booking-click' || goal === 'mobile-main-hero-menu-click' || goal === 'mobile-main-hero-wa-click' || goal === 'mobile-main-hero-phone-click') {
        yaMetricsSendEvent('mobile-main-first-screen-interaction', params);
      }
    }

    if (isLink) {
      setTimeout(
        function () {
          window.location.href = $el.attr('href');
        },
        100
      );
    }
  });

  // eslint-disable-next-line func-names
  $('.js-ga-goal').click(function (ev) {
    const $el = $(this);
    const isLink = $el.is('a') && $el.attr('target') !== '_blank';

    if (isLink) {
      ev.preventDefault();
    }

    let action = $el.data('ga-action');

    if (typeof action === 'undefined') {
      action = $el.data('goal');
    }

    const params = $el.data('ga-params');

    if (action) {
      gaSandEvent(action, params);

      // @todo Только на время проведения A/B-теста
      if (action === 'main-hero-booking-click' || action === 'mobile-main-hero-menu-click' || action === 'mobile-main-hero-wa-click' || action === 'mobile-main-hero-phone-click') {
        gaSandEvent('mobile-main-first-screen-interaction', params);
      }
    }

    if (isLink) {
      setTimeout(
        function () {
          window.location.href = $el.attr('href');
        },
        100
      );
    }
  });
}
