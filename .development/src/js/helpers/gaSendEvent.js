/* eslint-disable no-console */

import * as Cookies from "js-cookie";

export default function (action, params) {
  const debugMode = Cookies.get('debug');

  if (debugMode !== 'undefined') {
    console.log(`GA goal: ${action}`);

    if (params) {
      console.log(params);
    }
  }

  if (typeof gtag === 'undefined') {
    return;
  }

  if (params) {
    gtag('event', action, params);
  } else {
    gtag('event', action);
  }

  // Example
  // gtag('event', <action>, {
  //   'event_category': <category>,
  //   'event_label': <label>,
  //   'value': <value>
  // });
}
