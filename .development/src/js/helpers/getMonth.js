// Получение списка дней за определённый месяц
// Принимает объект даты

export default function (data) {
  const dictionary = {
    weekDays: [
      'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб',
    ]
  };

  function getDay(number, month, year) {
    const day = new Date(year, month, number);
    const type = day.getDay();

    return {
      number,
      title: dictionary.weekDays[type],
      date: day,
      dateString: day.toDateString(),
      weekDay: type,
      isAvailable: new Date().setDate(new Date().getDate() - 1) < day,
      isDayOff: type === 0 || type === 6,
      isToday: day.toDateString() === new Date().toDateString(),
    };
  }

  function buildMonth(date) {
    const month = date.getMonth();
    const year = date.getFullYear();
    const amountOfDays = (new Date(year, month + 1, 0)).getDate();

    const currentMonth = [];

    for (let i = 1; i <= amountOfDays; i += 1) {
      currentMonth.push(getDay(i, month, year));
    }

    return currentMonth;
  }

  return buildMonth(data);
}
